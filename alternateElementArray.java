package gfgCodes;
import java.util.Scanner;


public class alternateElementArray {
	public static void main(String args[])
	{

	int N,i=0,maximum,minimum;
	Scanner sc = new Scanner(System.in);
		N=sc.nextInt();
		int intArray[] = new int[N];
		for(i=0;i<N;i++)
		{
			intArray[i]=sc.nextInt();
		}
		
	//	System.out.println(N);
		
		for(i=0;i<N;i++)
		{
			System.out.print(intArray[i]+" ");
			i++;
		}
		sc.close();
		
		maximum=max(intArray,N);
		System.out.print("\nMaximum number in the array is: "+maximum);
	
		minimum=min(intArray,N);
		System.out.print("\nMinimum number in the array is: "+minimum);
	}
	
	public static int max(int[] a, int N)
	{
		int maximum=0,i,temp;
		for(i=0;i<N;i++)
		{
			if(a[i]>maximum)
			{
				temp=maximum;
				maximum=a[i];
			}
		}
		return maximum;
	}
	public static int min(int[] a, int N)
	{
		int minimum=a[0],i,temp;
		for(i=0;i<N;i++)
		{
			if(a[i]<minimum)
			{
				temp=minimum;
				minimum=a[i];
			}
		}
		return minimum;
	}
	
	
}

