package gfgCodes;

import java.util.Scanner;

public class NthTermofAp {

	public static void main(String args[])
	{
		int a1,a2,n,diff;
		System.out.println("A1=");
		Scanner sc= new Scanner(System.in);
		a1=sc.nextInt();
		System.out.println("A2=");
		a2=sc.nextInt();
		System.out.println("N=");
		n=sc.nextInt();
		sc.close();
		diff=a2-a1;
		System.out.println(diff*n+a1-diff);
	}
}
