package gfgCodes;

import java.time.*;
import java.util.*;
//import java.util.logging.*;
import java.text.*;

public class AgeCalculator {
	public static void main(String args[]) throws ParseException
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Date of birth");
		String date2=sc.next();
		System.out.println("Age at the date of");
		String date1=sc.next();
		sc.close();
		SimpleDateFormat formatted_date= new SimpleDateFormat("dd/MM/yyyy");
		Date dateformatof1=null;
		Date dateformatof2=null;
		dateformatof1=formatted_date.parse(date1);
		dateformatof2=formatted_date.parse(date2);
		
		long epoch_date1=dateformatof1.getTime(), epoch_date2=dateformatof2.getTime();
		long diff_time= epoch_date1-epoch_date2;
		if(diff_time>0)
		{
			int monthofdate1= dateformatof1.getMonth();
			int monthofdate2= dateformatof2.getMonth();
			int monthsdifference=(monthofdate1+monthofdate2+(dateformatof1.getYear()-dateformatof2.getYear())*12);
			monthsdifference=monthsdifference<25?monthsdifference/2:monthsdifference;
		long diff_in_seconds= (diff_time/1000);
		long diff_in_minutes= diff_time/(1000*60);
		long diff_in_hours= diff_time/(1000*60*60);
		long extradays_in_month =diff_time/(1000*60*60)%24;
		long diff_in_days= diff_time/(1000*60*60*24);
		long diff_in_weeks=diff_time/(1000*60*60*24*7);
		long extadays_in_weeks=diff_time/(1000*60*60*24)%7;
	//	long diff_in_months=diff_time/(1000*60*60*24*30); //leap year and months check
	//	System.out.println(diff_time);
		long diff_in_years= diff_time/(1000*60*60*24)*1/365;
	//	System.out.println(diff_in_years);
		System.out.println(diff_in_years+"years");
		System.out.println(monthsdifference+"months"+extradays_in_month+"days");
		System.out.println(diff_in_weeks+"weeks"+extadays_in_weeks+"days");
		System.out.println(diff_in_days+"days");
		System.out.println(diff_in_hours+" hours");
		System.out.println(diff_in_minutes+" minutes");
		System.out.println(diff_in_seconds+" Seconds");
		}
		else {
			System.out.println("From date should be greater then to date");
		}
		
	//	System.out.println(dateformatof1+"\n"+dateformatof2+"\n"+diff_time);
				
		
		
	}

}
