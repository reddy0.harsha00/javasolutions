package gfgCodes;

import java.util.Scanner;

public class SumOfdigitsofSum {
	public static void main(String args[])
	{
		int i=0,num,sum=0,op;
		Scanner sc=new Scanner(System.in);
		System.out.println("number of inputs");
		num=sc.nextInt();
		int[] numbers=new int[num];
		while(i<num)
		{
			numbers[i]=sc.nextInt();
			i++;
		}
		sc.close();
		
		for(i=0;i<num;i++)
		{
			sum=sum+numbers[i];
		}
		System.out.println("Sum of numbers: "+sum);
		
		op=sum(sum);
		//System.out.println("Final sum: "+op);
	}
	
	public static int sum(int snum)
	{
		int rem,temp=0,rec=0;
		
		while(snum!=0)
		{
			rem=snum%10;
			snum=snum/10;
			temp=temp+rem;
		}
		
		if(temp>9)
		{
			rec=rec+sum(temp);
		}
		else
		{
		System.out.println("Final sum: "+temp);
		}
		return temp;
	}

}
