package gfgCodes;
import java.util.*;

public class RhombusPattern {

	public static void main(String args[])
	{
		int i=0,num,temp=1;
		Boolean flag=false;
		System.out.println("enter the number");
		Scanner sc=new Scanner(System.in);
		num=sc.nextInt();
		sc.close();
		while(i<num+1)
		{
			i=i+temp;
			temp++;
			if(i==num)
			{
				flag=true;
				break;
			}
		}
		if(flag)
			Printrhombus(num,i);
		else
			System.out.println("Rhombus cannot be generated with the given number");
		
	}
	
	public static void Printrhombus(int n, int r)
	{
		int i,j,pv=1,count=0;
		
		for(i=0;i<=r;i++)
		   {
		     for(j=1;j<=r-i;j++)
		     System.out.print(" ");
		     count=0;
		     for(j=1;j<=2*i-1;j++,pv++)
		    	 if(j%2==1)
		    	 {
		    		 System.out.print(i+count);
		    		 count++;
		    	 }
		    	 else
		    		 System.out.print(" ");
		     
		     System.out.print("\n");
		   }
		 
		   for(i=r-1;i>=1;i--)
		   {
		     for(j=1;j<=r-i;j++)
		     System.out.print(" ");
		     for(j=1;j<=2*i-1;j++,pv++)
		       System.out.print(i);
		     System.out.print("\n");
		   }
	}
}
